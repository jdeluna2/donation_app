class Donor < ActiveRecord::Base
  has_many :donations
  validates_presence_of :name, :email
  validates_length_of :name, :minimum => 2
  validates_uniqueness_of :name



end
